/*
 * Ubiquity REST API
 *
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Protocols #### Mainnet The following protocols are currently supported: * bitcoin * ethereum * polkadot * xrp * algorand * stellar * dogecoin * oasis * near * litecoin * bitcoincash * tezos  #### Testnet * bitcoin/testnet * ethereum/ropsten * dogecoin/testnet * litecoin/testnet * bitcoincash/testnet  #### Native Ubiquity provides native access to all Blockchain nodes it supports. * bitcoin/(mainnet | testnet) - [RPC Documentation](https://developer.bitcoin.org/reference/rpc/) * ethereum/(mainnet | ropsten) - [RPC Documentation](https://ethereum.org/en/developers/docs/apis/json-rpc/) * polkadot/mainnet - [Sidecar API Documentation](https://paritytech.github.io/substrate-api-sidecar/dist/) * polkadot/mainnet/http-rpc - [Polkadot RPC Documentation](https://polkadot.js.org/docs/substrate/rpc/) * algorand/mainnet - [Algod API Documentation](https://developer.algorand.org/docs/reference/rest-apis/algod/) * stellar/mainnet - [Stellar Horizon API Documentation](https://developers.stellar.org/api) * dogecoin/(mainnet | testnet) - [Dogecoin API Documentaion](https://developer.bitcoin.org/reference/rpc/) * oasis/mainnet - [Oasis Rosetta Gateway Documentation](https://www.rosetta-api.org/docs/api_identifiers.html#network-identifier) * near/mainnet - [NEAR RPC Documentation](https://docs.near.org/docs/api/rpc) * litecoin/mainnet - [Litecoin RPC Documentation](https://litecoin.info/index.php/Litecoin_API) * bitcoincash/mainnet - [Bitcoin Cash RPC Documentation](https://docs.bitcoincashnode.org/doc/json-rpc/) * tezos/mainnet - [Tezos RPC Documentation](https://tezos.gitlab.io/developer/rpc.html)   A full URL example: https://ubiquity.api.blockdaemon.com/v1/bitcoin/mainnet  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available.
 *
 * API version: 3.0.0
 * Contact: support@blockdaemon.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package ubiquity

import (
	"encoding/json"
)

// PlatformsOverview struct for PlatformsOverview
type PlatformsOverview struct {
	// List of items each describing a pair of supported platform and network.
	Platforms *[]PlatformsOverviewPlatforms `json:"platforms,omitempty"`
}

// NewPlatformsOverview instantiates a new PlatformsOverview object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPlatformsOverview() *PlatformsOverview {
	this := PlatformsOverview{}
	return &this
}

// NewPlatformsOverviewWithDefaults instantiates a new PlatformsOverview object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPlatformsOverviewWithDefaults() *PlatformsOverview {
	this := PlatformsOverview{}
	return &this
}

// GetPlatforms returns the Platforms field value if set, zero value otherwise.
func (o *PlatformsOverview) GetPlatforms() []PlatformsOverviewPlatforms {
	if o == nil || o.Platforms == nil {
		var ret []PlatformsOverviewPlatforms
		return ret
	}
	return *o.Platforms
}

// GetPlatformsOk returns a tuple with the Platforms field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PlatformsOverview) GetPlatformsOk() (*[]PlatformsOverviewPlatforms, bool) {
	if o == nil || o.Platforms == nil {
		return nil, false
	}
	return o.Platforms, true
}

// HasPlatforms returns a boolean if a field has been set.
func (o *PlatformsOverview) HasPlatforms() bool {
	if o != nil && o.Platforms != nil {
		return true
	}

	return false
}

// SetPlatforms gets a reference to the given []PlatformsOverviewPlatforms and assigns it to the Platforms field.
func (o *PlatformsOverview) SetPlatforms(v []PlatformsOverviewPlatforms) {
	o.Platforms = &v
}

func (o PlatformsOverview) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Platforms != nil {
		toSerialize["platforms"] = o.Platforms
	}
	return json.Marshal(toSerialize)
}

type NullablePlatformsOverview struct {
	value *PlatformsOverview
	isSet bool
}

func (v NullablePlatformsOverview) Get() *PlatformsOverview {
	return v.value
}

func (v *NullablePlatformsOverview) Set(val *PlatformsOverview) {
	v.value = val
	v.isSet = true
}

func (v NullablePlatformsOverview) IsSet() bool {
	return v.isSet
}

func (v *NullablePlatformsOverview) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePlatformsOverview(val *PlatformsOverview) *NullablePlatformsOverview {
	return &NullablePlatformsOverview{value: val, isSet: true}
}

func (v NullablePlatformsOverview) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePlatformsOverview) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
