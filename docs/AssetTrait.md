# AssetTrait

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TraitType** | Pointer to **string** |  | [optional] 
**Value** | Pointer to **string** |  | [optional] 

## Methods

### NewAssetTrait

`func NewAssetTrait() *AssetTrait`

NewAssetTrait instantiates a new AssetTrait object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAssetTraitWithDefaults

`func NewAssetTraitWithDefaults() *AssetTrait`

NewAssetTraitWithDefaults instantiates a new AssetTrait object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTraitType

`func (o *AssetTrait) GetTraitType() string`

GetTraitType returns the TraitType field if non-nil, zero value otherwise.

### GetTraitTypeOk

`func (o *AssetTrait) GetTraitTypeOk() (*string, bool)`

GetTraitTypeOk returns a tuple with the TraitType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTraitType

`func (o *AssetTrait) SetTraitType(v string)`

SetTraitType sets TraitType field to given value.

### HasTraitType

`func (o *AssetTrait) HasTraitType() bool`

HasTraitType returns a boolean if a field has been set.

### GetValue

`func (o *AssetTrait) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *AssetTrait) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *AssetTrait) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *AssetTrait) HasValue() bool`

HasValue returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


