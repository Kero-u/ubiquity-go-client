# \AccountsAPI

All URIs are relative to *https://ubiquity.api.blockdaemon.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetListOfBalancesByAddress**](AccountsAPI.md#GetListOfBalancesByAddress) | **Get** /{platform}/{network}/account/{address} | Balances Of Address
[**GetListOfBalancesByAddresses**](AccountsAPI.md#GetListOfBalancesByAddresses) | **Post** /{platform}/{network}/accounts | Balances Of Addresses
[**GetReportByAddress**](AccountsAPI.md#GetReportByAddress) | **Get** /{platform}/{network}/account/{address}/report | A financial report for an address between a time period. Default timescale is within the last 30 days
[**GetTxsByAddress**](AccountsAPI.md#GetTxsByAddress) | **Get** /{platform}/{network}/account/{address}/txs | Transactions Of Address



## GetListOfBalancesByAddress

> []Balance GetListOfBalancesByAddress(ctx, platform, network, address).Assets(assets).Execute()

Balances Of Address



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    address := "0x2E31B312290A01538514806Fbb857736ea4d5555" // string | Account address
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetListOfBalancesByAddress(context.Background(), platform, network, address).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetListOfBalancesByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetListOfBalancesByAddress`: []Balance
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetListOfBalancesByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**address** | **string** | Account address | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetListOfBalancesByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

[**[]Balance**](Balance.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetListOfBalancesByAddresses

> map[string][]Balance GetListOfBalancesByAddresses(ctx, platform, network).AccountsObj(accountsObj).Assets(assets).Execute()

Balances Of Addresses



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    accountsObj := *openapiclient.NewAccountsObj() // AccountsObj | 
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetListOfBalancesByAddresses(context.Background(), platform, network).AccountsObj(accountsObj).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetListOfBalancesByAddresses``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetListOfBalancesByAddresses`: map[string][]Balance
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetListOfBalancesByAddresses`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetListOfBalancesByAddressesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accountsObj** | [**AccountsObj**](AccountsObj.md) |  | 
 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

[**map[string][]Balance**](array.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetReportByAddress

> Report GetReportByAddress(ctx, platform, network, address).From(from).To(to).Limit(limit).Continuation(continuation).Execute()

A financial report for an address between a time period. Default timescale is within the last 30 days



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    address := "0x2E31B312290A01538514806Fbb857736ea4d5555" // string | Account address
    from := int32(961846434) // int32 | Unix Timestamp from where to start (optional)
    to := int32(1119612834) // int32 | Unix Timestamp from where to end (optional)
    limit := int32(1000) // int32 | Max number of items to return in a response. Defaults to 50k and is capped at 100k.  (optional)
    continuation := "xyz" // string | Continuation token from earlier response (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetReportByAddress(context.Background(), platform, network, address).From(from).To(to).Limit(limit).Continuation(continuation).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetReportByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetReportByAddress`: Report
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetReportByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**address** | **string** | Account address | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetReportByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **from** | **int32** | Unix Timestamp from where to start | 
 **to** | **int32** | Unix Timestamp from where to end | 
 **limit** | **int32** | Max number of items to return in a response. Defaults to 50k and is capped at 100k.  | 
 **continuation** | **string** | Continuation token from earlier response | 

### Return type

[**Report**](Report.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTxsByAddress

> TxPage GetTxsByAddress(ctx, platform, network, address).Order(order).Continuation(continuation).Limit(limit).Assets(assets).Execute()

Transactions Of Address



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    address := "0x2E31B312290A01538514806Fbb857736ea4d5555" // string | Account address
    order := "order_example" // string | Pagination order (optional)
    continuation := "8185.123" // string | Continuation token from earlier response (optional)
    limit := int32(25) // int32 | Max number of items to return in a response. Defaults to 25 and is capped at 100.  (optional)
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetTxsByAddress(context.Background(), platform, network, address).Order(order).Continuation(continuation).Limit(limit).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetTxsByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTxsByAddress`: TxPage
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetTxsByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**address** | **string** | Account address | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTxsByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **order** | **string** | Pagination order | 
 **continuation** | **string** | Continuation token from earlier response | 
 **limit** | **int32** | Max number of items to return in a response. Defaults to 25 and is capped at 100.  | 
 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

[**TxPage**](TxPage.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

