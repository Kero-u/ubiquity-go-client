# TxConfirmation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CurrentHeight** | Pointer to **int64** | Current Block Number | [optional] 
**TxId** | Pointer to **string** | Transaction hash | [optional] 
**Confirmations** | Pointer to **int64** | Total transaction confirmations | [optional] 

## Methods

### NewTxConfirmation

`func NewTxConfirmation() *TxConfirmation`

NewTxConfirmation instantiates a new TxConfirmation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxConfirmationWithDefaults

`func NewTxConfirmationWithDefaults() *TxConfirmation`

NewTxConfirmationWithDefaults instantiates a new TxConfirmation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCurrentHeight

`func (o *TxConfirmation) GetCurrentHeight() int64`

GetCurrentHeight returns the CurrentHeight field if non-nil, zero value otherwise.

### GetCurrentHeightOk

`func (o *TxConfirmation) GetCurrentHeightOk() (*int64, bool)`

GetCurrentHeightOk returns a tuple with the CurrentHeight field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentHeight

`func (o *TxConfirmation) SetCurrentHeight(v int64)`

SetCurrentHeight sets CurrentHeight field to given value.

### HasCurrentHeight

`func (o *TxConfirmation) HasCurrentHeight() bool`

HasCurrentHeight returns a boolean if a field has been set.

### GetTxId

`func (o *TxConfirmation) GetTxId() string`

GetTxId returns the TxId field if non-nil, zero value otherwise.

### GetTxIdOk

`func (o *TxConfirmation) GetTxIdOk() (*string, bool)`

GetTxIdOk returns a tuple with the TxId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTxId

`func (o *TxConfirmation) SetTxId(v string)`

SetTxId sets TxId field to given value.

### HasTxId

`func (o *TxConfirmation) HasTxId() bool`

HasTxId returns a boolean if a field has been set.

### GetConfirmations

`func (o *TxConfirmation) GetConfirmations() int64`

GetConfirmations returns the Confirmations field if non-nil, zero value otherwise.

### GetConfirmationsOk

`func (o *TxConfirmation) GetConfirmationsOk() (*int64, bool)`

GetConfirmationsOk returns a tuple with the Confirmations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfirmations

`func (o *TxConfirmation) SetConfirmations(v int64)`

SetConfirmations sets Confirmations field to given value.

### HasConfirmations

`func (o *TxConfirmation) HasConfirmations() bool`

HasConfirmations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


