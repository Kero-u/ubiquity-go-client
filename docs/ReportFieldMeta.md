# ReportFieldMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** |  | [optional] [default to "algorand_meta"]
**SenderReward** | Pointer to **string** |  | [optional] 
**RecipientReward** | Pointer to **string** |  | [optional] 
**Close** | Pointer to **string** |  | [optional] 
**CloseAmount** | Pointer to **string** |  | [optional] 
**CloseReward** | Pointer to **string** |  | [optional] 

## Methods

### NewReportFieldMeta

`func NewReportFieldMeta() *ReportFieldMeta`

NewReportFieldMeta instantiates a new ReportFieldMeta object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportFieldMetaWithDefaults

`func NewReportFieldMetaWithDefaults() *ReportFieldMeta`

NewReportFieldMetaWithDefaults instantiates a new ReportFieldMeta object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *ReportFieldMeta) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ReportFieldMeta) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ReportFieldMeta) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ReportFieldMeta) HasType() bool`

HasType returns a boolean if a field has been set.

### GetSenderReward

`func (o *ReportFieldMeta) GetSenderReward() string`

GetSenderReward returns the SenderReward field if non-nil, zero value otherwise.

### GetSenderRewardOk

`func (o *ReportFieldMeta) GetSenderRewardOk() (*string, bool)`

GetSenderRewardOk returns a tuple with the SenderReward field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderReward

`func (o *ReportFieldMeta) SetSenderReward(v string)`

SetSenderReward sets SenderReward field to given value.

### HasSenderReward

`func (o *ReportFieldMeta) HasSenderReward() bool`

HasSenderReward returns a boolean if a field has been set.

### GetRecipientReward

`func (o *ReportFieldMeta) GetRecipientReward() string`

GetRecipientReward returns the RecipientReward field if non-nil, zero value otherwise.

### GetRecipientRewardOk

`func (o *ReportFieldMeta) GetRecipientRewardOk() (*string, bool)`

GetRecipientRewardOk returns a tuple with the RecipientReward field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecipientReward

`func (o *ReportFieldMeta) SetRecipientReward(v string)`

SetRecipientReward sets RecipientReward field to given value.

### HasRecipientReward

`func (o *ReportFieldMeta) HasRecipientReward() bool`

HasRecipientReward returns a boolean if a field has been set.

### GetClose

`func (o *ReportFieldMeta) GetClose() string`

GetClose returns the Close field if non-nil, zero value otherwise.

### GetCloseOk

`func (o *ReportFieldMeta) GetCloseOk() (*string, bool)`

GetCloseOk returns a tuple with the Close field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetClose

`func (o *ReportFieldMeta) SetClose(v string)`

SetClose sets Close field to given value.

### HasClose

`func (o *ReportFieldMeta) HasClose() bool`

HasClose returns a boolean if a field has been set.

### GetCloseAmount

`func (o *ReportFieldMeta) GetCloseAmount() string`

GetCloseAmount returns the CloseAmount field if non-nil, zero value otherwise.

### GetCloseAmountOk

`func (o *ReportFieldMeta) GetCloseAmountOk() (*string, bool)`

GetCloseAmountOk returns a tuple with the CloseAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCloseAmount

`func (o *ReportFieldMeta) SetCloseAmount(v string)`

SetCloseAmount sets CloseAmount field to given value.

### HasCloseAmount

`func (o *ReportFieldMeta) HasCloseAmount() bool`

HasCloseAmount returns a boolean if a field has been set.

### GetCloseReward

`func (o *ReportFieldMeta) GetCloseReward() string`

GetCloseReward returns the CloseReward field if non-nil, zero value otherwise.

### GetCloseRewardOk

`func (o *ReportFieldMeta) GetCloseRewardOk() (*string, bool)`

GetCloseRewardOk returns a tuple with the CloseReward field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCloseReward

`func (o *ReportFieldMeta) SetCloseReward(v string)`

SetCloseReward sets CloseReward field to given value.

### HasCloseReward

`func (o *ReportFieldMeta) HasCloseReward() bool`

HasCloseReward returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


