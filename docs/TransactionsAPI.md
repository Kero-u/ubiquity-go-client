# \TransactionsAPI

All URIs are relative to *https://ubiquity.api.blockdaemon.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**FeeEstimate**](TransactionsAPI.md#FeeEstimate) | **Get** /{platform}/{network}/tx/estimate_fee | Get fee estimate
[**GetTx**](TransactionsAPI.md#GetTx) | **Get** /{platform}/{network}/tx/{id} | Transaction By Hash
[**GetTxByHashAndIndex**](TransactionsAPI.md#GetTxByHashAndIndex) | **Get** /{platform}/{network}/tx/{id}/{index} | Transaction output by hash and index
[**GetTxConfirmations**](TransactionsAPI.md#GetTxConfirmations) | **Get** /{platform}/{network}/tx/{id}/confirmations | Transaction confirmations By Hash
[**GetTxs**](TransactionsAPI.md#GetTxs) | **Get** /{platform}/{network}/txs | Latest transactions of a protocol
[**TxSend**](TransactionsAPI.md#TxSend) | **Post** /{platform}/{network}/tx/send | Submit a signed transaction



## FeeEstimate

> FeeEstimate FeeEstimate(ctx, platform, network).Execute()

Get fee estimate



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransactionsAPI.FeeEstimate(context.Background(), platform, network).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransactionsAPI.FeeEstimate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `FeeEstimate`: FeeEstimate
    fmt.Fprintf(os.Stdout, "Response from `TransactionsAPI.FeeEstimate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 

### Other Parameters

Other parameters are passed through a pointer to a apiFeeEstimateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**FeeEstimate**](FeeEstimate.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTx

> Tx GetTx(ctx, platform, network, id).Execute()

Transaction By Hash

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    id := "0xF00Fa860473130C1df10707223E66Cb4B839B165" // string | Transaction ID/Hash

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransactionsAPI.GetTx(context.Background(), platform, network, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransactionsAPI.GetTx``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTx`: Tx
    fmt.Fprintf(os.Stdout, "Response from `TransactionsAPI.GetTx`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**id** | **string** | Transaction ID/Hash | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTxRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Tx**](Tx.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTxByHashAndIndex

> TxOutput GetTxByHashAndIndex(ctx, platform, network, id, index).Execute()

Transaction output by hash and index

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    id := "0xF00Fa860473130C1df10707223E66Cb4B839B165" // string | Transaction ID/Hash
    index := int32(0) // int32 | Transaction output index

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransactionsAPI.GetTxByHashAndIndex(context.Background(), platform, network, id, index).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransactionsAPI.GetTxByHashAndIndex``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTxByHashAndIndex`: TxOutput
    fmt.Fprintf(os.Stdout, "Response from `TransactionsAPI.GetTxByHashAndIndex`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**id** | **string** | Transaction ID/Hash | 
**index** | **int32** | Transaction output index | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTxByHashAndIndexRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------





### Return type

[**TxOutput**](TxOutput.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTxConfirmations

> TxConfirmation GetTxConfirmations(ctx, platform, network, id).Execute()

Transaction confirmations By Hash

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    id := "0xF00Fa860473130C1df10707223E66Cb4B839B165" // string | Transaction ID/Hash

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransactionsAPI.GetTxConfirmations(context.Background(), platform, network, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransactionsAPI.GetTxConfirmations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTxConfirmations`: TxConfirmation
    fmt.Fprintf(os.Stdout, "Response from `TransactionsAPI.GetTxConfirmations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**id** | **string** | Transaction ID/Hash | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTxConfirmationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**TxConfirmation**](TxConfirmation.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTxs

> TxPage GetTxs(ctx, platform, network).Order(order).Continuation(continuation).Limit(limit).Assets(assets).Execute()

Latest transactions of a protocol



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    order := "order_example" // string | Pagination order (optional)
    continuation := "8185.123" // string | Continuation token from earlier response (optional)
    limit := int32(25) // int32 | Max number of items to return in a response. Defaults to 25 and is capped at 100.  (optional)
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransactionsAPI.GetTxs(context.Background(), platform, network).Order(order).Continuation(continuation).Limit(limit).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransactionsAPI.GetTxs``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTxs`: TxPage
    fmt.Fprintf(os.Stdout, "Response from `TransactionsAPI.GetTxs`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTxsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **order** | **string** | Pagination order | 
 **continuation** | **string** | Continuation token from earlier response | 
 **limit** | **int32** | Max number of items to return in a response. Defaults to 25 and is capped at 100.  | 
 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

[**TxPage**](TxPage.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## TxSend

> TxReceipt TxSend(ctx, platform, network).SignedTx(signedTx).Execute()

Submit a signed transaction



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    signedTx := *openapiclient.NewSignedTx("0100000001ca19af5fb94ced7e62b623d0039a398a42e60050405a1341efe475894629c131010000008b483045022100d77b002b3142013b3f825a730f5bc3ead2014266f07ba4449269af0cf6f086310220365bca1d616ba86fac42ad69efd5f92c5ed6cf16f27ebf5ab55010efc72c219d014104417eb0abe69db2eca63c84eb44266c29c24973dc81cde16ca86c9d923630cb5f797bae7d7fab13498e06146111356eb271da74add05ebda8f72ff2b2878fddb7ffffffff0410270000000000001976a914344a0f48ca150ec2b903817660b9b68b13a6702688ac204e0000000000001976a914344a0f48ca150ec2b903817660b9b68b13a6702688ac30750000000000001976a914344a0f48ca150ec2b903817660b9b68b13a6702688ac48710000000000001976a914d6fa8814924b480fa7ff903b5ef61100ab4d92fe88ac00000000") // SignedTx | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransactionsAPI.TxSend(context.Background(), platform, network).SignedTx(signedTx).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransactionsAPI.TxSend``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `TxSend`: TxReceipt
    fmt.Fprintf(os.Stdout, "Response from `TransactionsAPI.TxSend`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 

### Other Parameters

Other parameters are passed through a pointer to a apiTxSendRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **signedTx** | [**SignedTx**](SignedTx.md) |  | 

### Return type

[**TxReceipt**](TxReceipt.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

