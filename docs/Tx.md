# Tx

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Unique transaction identifier | [optional] 
**Date** | Pointer to **int64** | Unix timestamp | [optional] 
**BlockId** | Pointer to **NullableString** | ID of block if mined, otherwise omitted. | [optional] 
**Status** | Pointer to **string** | Result status of the transaction. | [optional] 
**Assets** | Pointer to **[]string** | List of moved assets by asset path | [optional] 
**Nonce** | Pointer to **int32** |  | [optional] 
**NumEvents** | Pointer to **int32** |  | [optional] 
**Meta** | Pointer to **interface{}** |  | [optional] 
**Events** | Pointer to [**[]Event**](Event.md) |  | [optional] 

## Methods

### NewTx

`func NewTx() *Tx`

NewTx instantiates a new Tx object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxWithDefaults

`func NewTxWithDefaults() *Tx`

NewTxWithDefaults instantiates a new Tx object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Tx) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Tx) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Tx) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *Tx) HasId() bool`

HasId returns a boolean if a field has been set.

### GetDate

`func (o *Tx) GetDate() int64`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *Tx) GetDateOk() (*int64, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *Tx) SetDate(v int64)`

SetDate sets Date field to given value.

### HasDate

`func (o *Tx) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetBlockId

`func (o *Tx) GetBlockId() string`

GetBlockId returns the BlockId field if non-nil, zero value otherwise.

### GetBlockIdOk

`func (o *Tx) GetBlockIdOk() (*string, bool)`

GetBlockIdOk returns a tuple with the BlockId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockId

`func (o *Tx) SetBlockId(v string)`

SetBlockId sets BlockId field to given value.

### HasBlockId

`func (o *Tx) HasBlockId() bool`

HasBlockId returns a boolean if a field has been set.

### SetBlockIdNil

`func (o *Tx) SetBlockIdNil(b bool)`

 SetBlockIdNil sets the value for BlockId to be an explicit nil

### UnsetBlockId
`func (o *Tx) UnsetBlockId()`

UnsetBlockId ensures that no value is present for BlockId, not even an explicit nil
### GetStatus

`func (o *Tx) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Tx) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Tx) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *Tx) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetAssets

`func (o *Tx) GetAssets() []string`

GetAssets returns the Assets field if non-nil, zero value otherwise.

### GetAssetsOk

`func (o *Tx) GetAssetsOk() (*[]string, bool)`

GetAssetsOk returns a tuple with the Assets field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssets

`func (o *Tx) SetAssets(v []string)`

SetAssets sets Assets field to given value.

### HasAssets

`func (o *Tx) HasAssets() bool`

HasAssets returns a boolean if a field has been set.

### SetAssetsNil

`func (o *Tx) SetAssetsNil(b bool)`

 SetAssetsNil sets the value for Assets to be an explicit nil

### UnsetAssets
`func (o *Tx) UnsetAssets()`

UnsetAssets ensures that no value is present for Assets, not even an explicit nil
### GetNonce

`func (o *Tx) GetNonce() int32`

GetNonce returns the Nonce field if non-nil, zero value otherwise.

### GetNonceOk

`func (o *Tx) GetNonceOk() (*int32, bool)`

GetNonceOk returns a tuple with the Nonce field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNonce

`func (o *Tx) SetNonce(v int32)`

SetNonce sets Nonce field to given value.

### HasNonce

`func (o *Tx) HasNonce() bool`

HasNonce returns a boolean if a field has been set.

### GetNumEvents

`func (o *Tx) GetNumEvents() int32`

GetNumEvents returns the NumEvents field if non-nil, zero value otherwise.

### GetNumEventsOk

`func (o *Tx) GetNumEventsOk() (*int32, bool)`

GetNumEventsOk returns a tuple with the NumEvents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumEvents

`func (o *Tx) SetNumEvents(v int32)`

SetNumEvents sets NumEvents field to given value.

### HasNumEvents

`func (o *Tx) HasNumEvents() bool`

HasNumEvents returns a boolean if a field has been set.

### GetMeta

`func (o *Tx) GetMeta() interface{}`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *Tx) GetMetaOk() (*interface{}, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *Tx) SetMeta(v interface{})`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *Tx) HasMeta() bool`

HasMeta returns a boolean if a field has been set.

### SetMetaNil

`func (o *Tx) SetMetaNil(b bool)`

 SetMetaNil sets the value for Meta to be an explicit nil

### UnsetMeta
`func (o *Tx) UnsetMeta()`

UnsetMeta ensures that no value is present for Meta, not even an explicit nil
### GetEvents

`func (o *Tx) GetEvents() []Event`

GetEvents returns the Events field if non-nil, zero value otherwise.

### GetEventsOk

`func (o *Tx) GetEventsOk() (*[]Event, bool)`

GetEventsOk returns a tuple with the Events field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvents

`func (o *Tx) SetEvents(v []Event)`

SetEvents sets Events field to given value.

### HasEvents

`func (o *Tx) HasEvents() bool`

HasEvents returns a boolean if a field has been set.

### SetEventsNil

`func (o *Tx) SetEventsNil(b bool)`

 SetEventsNil sets the value for Events to be an explicit nil

### UnsetEvents
`func (o *Tx) UnsetEvents()`

UnsetEvents ensures that no value is present for Events, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


